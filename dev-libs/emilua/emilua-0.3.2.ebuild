EAPI=8
EGIT_REPO_URI="https://gitlab.com/emilua/emilua.git"
inherit git-r3
inherit meson
DESCRIPTION="Lua execution engine"


HOMEPAGE="http://emilua.org/"

SRC_URI="https://gitlab.com/emilua/emilua.git"



LICENSE="boost"

SLOT="0"

KEYWORDS="~amd64"

IUSE=""
RDEPEND="
       dev-lang/luajit[lua52compat]
"


DEPEND="
       dev-libs/libfmt
	   sys-libs/ncurses
	   dev-ruby/asciidoctor
	   sys-apps/gawk
"

BDEPEND="
        dev-cpp/boost 
        dev-util/re2c
        app-misc/xxd
"
